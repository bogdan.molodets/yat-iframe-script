# YAT script
___
This script must be placed in html, that contain div with `target` id.
For custom ids change this field of code:
~~~
document.getElementById('target').appendChild(iframe);
~~~
Example: 
~~~
document.getElementById('myCustomId').appendChild(iframe);
~~~
Iframe by default use `width` & `height` properties of its **parent block**. For customize, change this lines in source code:

~~~
iframe.width = "100%";
iframe.height = "100%";
~~~
Example: 
~~~
iframe.width = "300px";
iframe.height = "90%";
~~~
There is `example` of script usage in **index.html**.
___
#### Source code:

```javascript
<script async defer>
	window.onload = function(){
		const options = {
        	enableHighAccuracy: true,
        	timeout: 5000,
        	maximumAge: 0
      	};ы

		navigator.geolocation.getCurrentPosition(success, error, options);

	}

	function success(res){
		setIframe(`https://partner.yourairtest.com/?lng=${res.coords.longitude}&lat=${res.coords.latitude}&zoom=7&lang=${getLang()}`);

	}

	function error(){
		setIframe(`https://partner.yourairtest.com/?lang=${getLang()}`);

	}

	function setIframe(url){
		const  iframe = document.createElement('iframe');
		iframe.frameBorder = 0;
		iframe.width = "100%";
		iframe.height = "100%";
		iframe.setAttribute("src", url);
		document.getElementById('target').appendChild(iframe);

	}

	function getLang(){
		const pathNameParts = window.location.pathname.split('/');
		if(pathNameParts.includes('en')){
			return 'en';
		} else if (pathNameParts.includes('ru')){
			return 'ru';
		} else {ы
			return 'uk';
		}
	}

</script>
```
